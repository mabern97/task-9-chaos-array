Type: Console Application

Allow a user to Insert and Retrieve words or numbers from a custom collection called ChaosArray.

Requirements:

·         Demonstrate the use of a custom collection called called ChaosArray

·         It should be able to store any type

·         There should only be one way to insert an item in the collection – which is inserted randomly

·         There should only be one way to retrieve an item in the collection – which returns a random item

·         Use a custom Exception to throw in instances where an item is being inserted into an unavailable space and when an attempt to retrieve an item that is not there

·         Catch the custom exception

Hint: Make use of an array based custom collection. Want to avoid lists as their size is variable – never inserting randomly.

Learn C#: Generics

Weight: Basic

