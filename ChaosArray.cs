﻿using System;
using System.Collections.Generic;

namespace Task9ChaosArray
{
    public class ChaosArray
    {
        #region Variables
        private object[] array; // Array that contains all the data
        private Random random; // The randomization 'engine'
        #endregion

        #region Properties
        public int Length { get; private set; } // Provides the length of the collection
        #endregion

        #region Constructors
        public ChaosArray(int length = 10) // Default initialized length will be 10 items
        {
            Length = length;
            array = new object[Length];
            random = new Random();
        }
        #endregion

        #region Public Methods
        /*
         * Insert any object into the array,
         * only if there's available space
         * in the array.
         */
        public void Insert(params object[] contents)
        {
            foreach(object item in contents)
            {
                int index = GetIndex();
                if (!IsIndexAllocated(index))
                    array[index] = item;
            }
        }

        /*
         * Removes a random item from the array,
         * and then returns the item removed
         * from the array.
         * 
         * Will also return the item that was removed.
         */
        public object Remove()
        {
            int index = GetIndex(true);
            object value = array[index];

            array[index] = default(object);

            return value;
        }

        /*
         * Returns a random object from the array.
         */
        public object Retrieve()
        {
            int index = GetIndex(true);

            return array[index];
        }
        #endregion

        #region Private Methods
        /*
         * Returns an index from the array, giving the option
         * to only get indexes that are free or in use.
         */
        private int GetIndex(bool allocated = false)
        {
            List<int> indexes = new List<int>();

            for (int i = 0; i < Length; i++)
                if (allocated && IsIndexAllocated(i))
                    indexes.Add(i);
                else if (!allocated && !IsIndexAllocated(i))
                    indexes.Add(i);

            if (indexes.Count == 0)
                if (!allocated)
                    throw new AllocatedArrayException("The chaos array is completely full");
                else
                    throw new EmptyArrayException("The chaos array has no objects inside");

            return indexes[random.Next(indexes.Count)];
        }

        /*
         * Checks if an index is categorized as allocated or not
         */
        private bool IsIndexAllocated(int index)
        {
            return array[index] != default(object);
        }
        #endregion
    }
}
