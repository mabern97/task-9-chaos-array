﻿using System;
using System.Collections.Generic;

namespace Task9ChaosArray
{
    class Program
    {
        static ChaosArray chaos;

        static void Main(string[] args)
        {
            // Create a chaos array with a length of 10
            chaos = new ChaosArray(5); // Create an array with 10 slots

            // Some helpful instructions for the user
            Print($"Welcome to Chaos City! We have a storage facility with {chaos.Length} slots", ConsoleColor.DarkMagenta);
            Print($"The items you store in this facility will be automatically placed in random order", ConsoleColor.DarkMagenta);
            Print($"This storage facility will not let you manually view or remove a specific item, sorry.", ConsoleColor.DarkMagenta);
            Print($"\nListed below are your options:", ConsoleColor.DarkMagenta);
            Print($" - add [item] (Example: add cadbury egg)", ConsoleColor.DarkGreen);
            Print($" - remove (Randomly removes any item from storage)", ConsoleColor.DarkRed);
            Print($" - retrieve (Randomly retrieves any item from storage)", ConsoleColor.DarkYellow);
            Print($" - exit (Quit the application)", ConsoleColor.Red);
            Print($"\n");

            bool run = true;

            error:
            try
            {
                while (run)
                    run = ParseCommands();
            }
            catch(Exception ex)
            {
                Print($"[ERROR] {ex.Message}", ConsoleColor.Red);
                goto error;
            }
        }


        /*
         * ParseCommands
         * Interpret what the user is inputting into the console window and perform
         * what the user wants to perform.
         */
        static bool ParseCommands()
        {
            Console.Write($"> ");
            string input = Console.ReadLine();

            bool run = true;

            switch(input)
            {
                // Remove an item from the ChaosArray
                case "remove":
                    object removed = chaos.Remove();
                    Print($"Removed '{removed.ToString()}'", ConsoleColor.DarkRed);
                    break;
                // Retrieve an item from the ChaosArray
                case "retrieve":
                    object retrieved = chaos.Retrieve();
                    Print($"Retrieved '{retrieved.ToString()} (Type: {retrieved.GetType()})'", ConsoleColor.DarkYellow);
                    break;
                // Quit the application
                case "exit":
                    run = false;
                    break;

                // Add an item (if the user has used the 'add' prefix)
                default:
                    // Add stuff
                    string[] contents = input.Split(" ");
                    if (contents[0].Equals("add"))
                    {
                        string item = input.Remove(0, 4);

                        /*
                         * Temporary variables holding different values of converted objects
                         */
                        string type = "String";

                        if (item.Contains(',')) // Treat comma separated values as an array
                        {
                            string[] arrayElems = item.Split(',');

                            object[] arr = new object[arrayElems.Length];
                            int i = 0;
                            foreach(object obj in arrayElems)
                            {
                                if (Int32.TryParse(item, out int objInt))
                                {
                                    arr[i] = objInt;
                                }
                                else if (Double.TryParse(item, out double objDouble))
                                {
                                    arr[i] = objDouble;
                                }
                                else if (Char.TryParse(item, out char objChar))
                                {
                                    arr[i] = objChar;
                                }
                                else if (Boolean.TryParse(item, out bool objBool))
                                {
                                    arr[i] = objBool;
                                }
                                else
                                    arr[i] = obj;

                                i++;
                            }

                            chaos.Insert(arr);
                            Print($"Added an array '[{item}]' to the chaos array.", ConsoleColor.DarkGreen);

                            break;
                        }


                        if (Int32.TryParse(item, out int tempInt))
                        {
                            type = "Integer";
                            chaos.Insert(tempInt);
                        }
                        else if (Double.TryParse(item, out double tempDouble))
                        {
                            type = "Double";
                            chaos.Insert(tempDouble);
                        }
                        else if (Char.TryParse(item, out char tempChar))
                        {
                            type = "Char";
                            chaos.Insert(tempChar);
                        }
                        else if (Boolean.TryParse(item, out bool tempBoolean))
                        {
                            type = "Boolean";
                            chaos.Insert(tempBoolean);
                        }
                        else
                            chaos.Insert(item);

                        Print($"Added '{item}' to the array as a {type}.", ConsoleColor.DarkGreen);

                        if (contents.Length <= 1)
                        {
                            throw new Exception("You haven't specified any item to add.");
                        }
                    }
                    break;
            }

            return run;
        }

        /*
         * Print out content to the console window, with a parameter to set color :-)
         */
        static void Print(string line, ConsoleColor color = ConsoleColor.White)
        {
            SetConsoleColor(color);
            Console.WriteLine(line);
            SetConsoleColor();
        }

        static void SetConsoleColor(ConsoleColor color = ConsoleColor.White)
        {
            Console.ForegroundColor = color;
        }
    }
}
