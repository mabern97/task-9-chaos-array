﻿using System;
namespace Task9ChaosArray
{
    /*
     * Exception thrown if the ChaosArray is completely empty.
     * Example usage: If trying to retrieve or remove an item
     *                from an empty array
     */
    public class EmptyArrayException : Exception
    {
        public EmptyArrayException()
        {
        }

        public EmptyArrayException(string message)
            : base(message)
        {

        }

        public EmptyArrayException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }

    /*
     * Exception thrown if the ChaosArray is completely full.
     * Example usage: If trying to insert and item into a array
     *                which is completely full
     */
    public class AllocatedArrayException : Exception
    {
        public AllocatedArrayException()
        {
        }

        public AllocatedArrayException(string message)
            : base(message)
        {

        }

        public AllocatedArrayException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
